<?php

use App\CommentInserterService;
use App\FileLogger;
use App\PDO\Connection;
use App\Repository\Comments;
use App\Repository\TVCook;

require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$config = include 'config.php';
$logger = new FileLogger();

function connectCommentDb(array $config): PDO
{
    $connection = new Connection($config['comment']['host'], $config['comment']['dbname'], $config['comment']['username'], $config['comment']['password']);

    return $connection->getConnection();
}

function connectTVCookDb(array $config): PDO
{
    $connection = new Connection($config['tvcook']['host'], $config['tvcook']['dbname'], $config['tvcook']['username'], $config['tvcook']['password']);

    return $connection->getConnection();
}

$tvcookConnection = connectTVCookDb($config);
$commentConnection = connectCommentDb($config);

$commentInserter = new CommentInserterService($logger, new TVCook($tvcookConnection), new Comments($commentConnection));
$commentInserter->uploadComments($config['commentsUploadLimit']);
