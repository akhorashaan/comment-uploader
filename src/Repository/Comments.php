<?php

declare(strict_types=1);

namespace App\Repository;

use App\DTO\Comment;
use App\DTO\User;
use Exception;
use PDO;

class Comments
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws Exception
     */
    public function getComment()
    {
        $stmt = $this->connection->query("SELECT * FROM comment WHERE posted IS NULL AND tvcook_id IS NOT NULL ORDER BY RAND() LIMIT 1");
        $comment = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$comment) {
            throw new Exception('Good comment was not found.');
        }

        return $comment;
    }

    /**
     * @throws Exception
     */
    public function getUser(): User
    {
        $stmt = $this->connection->query("SELECT * FROM users WHERE used IS false ORDER BY RAND() LIMIT 1");
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$user) {
            throw new Exception('Good user was not found.');
        }

        return new User($user);
    }

    public function makeUserUsed(User $user): void
    {
        $id = $user->getId();
        $stmt = $this->connection->prepare("UPDATE users SET used = true WHERE id = :id");
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }

    public function makeCommentUsed(Comment $comment): void
    {
        $id = $comment->getId();
        $date = date('Y-m-d h:m:s');
        $stmt = $this->connection->prepare("UPDATE comment SET posted = :date WHERE id = :id");
        $stmt->bindParam('id', $id);
        $stmt->bindParam('date', $date);
        $stmt->execute();
    }

    /**
     * @return Comment[]
     *
     * @throws Exception
     */
    public function getAllTVCookPresentedComments(): array
    {
        $data = $this->connection->query("SELECT * FROM comment WHERE tvcook_id IS NOT null")->fetchAll(PDO::FETCH_ASSOC);
        $comments = [];

        foreach ($data as $entry) {
            $comments[] = new Comment($entry);
        }

        return $comments;
    }

    public function updateFixedComment(Comment $comment, string $fix): void
    {
        $id = $comment->getId();
        $stmt = $this->connection->prepare("UPDATE comment SET comment = :comment WHERE id = :id");
        $stmt->bindParam('id', $id);
        $stmt->bindParam('comment', $fix);
        $stmt->execute();
    }
}
