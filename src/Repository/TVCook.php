<?php

declare(strict_types=1);

namespace App\Repository;

use App\DTO\Comment;
use App\DTO\User;
use App\FileLogger;
use Exception;
use PDO;
use Throwable;

class TVCook
{
    private $connection;
    private $logger;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
        $this->logger = new FileLogger();
    }

    /**
     * @throws Exception
     */
    public function createUser(User $user): int
    {
        $name = $user->getName();
        $mail = $name . '@user.tvcook.ru';
        $date = date('Y-m-d h:m:s');

        try {
            $st = $this->connection->prepare("
            INSERT INTO wqwe_user (
                               user_login,
                               user_password,
                               user_mail,
                               user_date_register,
                               user_date_activate,
                               user_ip_register, 
                               user_activate,
                               user_activate_key, 
                               user_profile_name
                               ) 
            VALUES (
                               :user_login,
                               '9c35076aae94638f160792ee5671fdde',
                               :user_mail,
                               :user_date_register,
                               :user_date_activate,
                               '127.0.0.1', 
                               true,
                               '71e4b82c16772b35f0c7e90189524fa5', 
                               :user_profile_name
            )");
            $st->bindParam('user_login', $name);
            $st->bindParam('user_mail', $mail);
            $st->bindParam('user_date_register', $date);
            $st->bindParam('user_date_activate', $date);
            $st->bindParam('user_profile_name', $name);
            $st->execute();

            $userId = $this->getUserId($name);
            $this->logger->log('info', "Created user with name: $name and id $userId");
        } catch (Throwable $e) {
            throw new Exception("Error while creating new user with name: $name  $e->getMessage()");
        }

        return $userId;
    }

    /**
     * @throws Exception
     */
    private function getUserId(string $login): int
    {
        $stmt = $this->connection->prepare("SELECT * FROM wqwe_user WHERE user_login = :user_login ORDER BY user_date_register LIMIT 1");
        $stmt->bindParam('user_login', $login);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$user) {
            throw new Exception("User was created with login: $login ,but not found.");
        }

        return (int)$user['user_id'];
    }

    /**
     * @throws Exception
     */
    public function createComment(Comment $comment, User $user): void
    {
        $tvCookId = $comment->getTVCookRecipeId();
        $commentText = $comment->getText();
        $hash = md5($commentText);
        $date = date('Y-m-d h:m:s');
        $userId = $user->getTVCookId();

        try {
            $stmt = $this->connection->prepare("
                INSERT INTO wqwe_comment (
                                      target_id, 
                                      target_type,
                                      user_id,
                                      comment_text, 
                                      comment_text_hash,
                                      comment_date, 
                                      comment_user_ip
                                      ) 
                VALUES (
                    :target_id, 
                    'topic', 
                    :user_id,
                    :comment_text,
                    :comment_text_hash,
                    :comment_date, 
                    '127.0.0.1'
            )");
            $stmt->bindParam('target_id', $tvCookId);
            $stmt->bindParam('user_id', $userId);
            $stmt->bindParam('comment_text', $commentText);
            $stmt->bindParam('comment_text_hash', $hash);
            $stmt->bindParam('comment_date', $date);
            $stmt->bindParam('target_id', $tvCookId);
            $stmt->execute();
            $this->logger->log('info', "Created comment with text: $commentText for recipe $tvCookId");
        } catch (Throwable $e) {
            throw new Exception("Error while creating new user with name: $commentText for recipe $tvCookId $e->getMessage()");
        }
    }
}
