<?php

namespace App;

use App\Curl\Spellchecker;
use App\Repository\Comments;
use Exception;

class CommentsSpellFixer
{
    private $commentsRepo;
    private $spellchecker;

    public function __construct(Comments $commentsRepo, Spellchecker $spellchecker)
    {
        $this->commentsRepo = $commentsRepo;
        $this->spellchecker = $spellchecker;
    }

    /**
     * @throws Exception
     */
    public function checkAndFix(): void
    {
        $comments = $this->commentsRepo->getAllTVCookPresentedComments();

        foreach ($comments as $comment) {
            $string = $comment->getText();

            $errors = $this->spellchecker->check($string);

            if (!empty($errors)) {
                $fixed = $this->correct($string, $errors);
                $this->commentsRepo->updateFixedComment($comment, $fixed);
            }
        }
    }

    private function correct(string $string, array $errors): string
    {
        $corrections = [];

        foreach ($errors as $error) {
            $badWord = mb_substr($string, $error->pos, $error->len);
            $suggestions = $error->s;
            $goodWord = $suggestions[0];
            $corrections[] = ['bad' => $badWord, 'good' => $goodWord];
        }

        if (!empty($corrections)) {
            foreach ($corrections as $correction) {
                $string = str_replace($correction['bad'], $correction['good'], $string);
            }
        }

        return $string;
    }
}