<?php

declare(strict_types=1);

namespace App;

use Psr\Log\AbstractLogger;

final class FileLogger extends AbstractLogger
{
    public function log($level, $message, array $context = []): void
    {
        $file = $this->getFilePath((string)$level);

        file_put_contents($file, $this->formatMessage($message), FILE_APPEND);
    }

    private function getFilePath(string $level): string
    {
        return $this->logsPath() . DIRECTORY_SEPARATOR . $level . '-log.txt';
    }

    private function logsPath(): string
    {
        return dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR;
    }

    private function formatMessage(string $message): string
    {
        return date('Y-m-d H:i:s') . ' ' . $message . "\n";
    }
}
