<?php

declare(strict_types=1);

namespace App\DTO;

use Exception;

class User
{
    private $name;
    private $commentsID;
    private $tvcookId;

    /**
     * @throws Exception
     */
    public function __construct(array $creds)
    {
        if (!array_key_exists('id', $creds) || !array_key_exists('name', $creds)) {
            throw new Exception('Fetched user has no id or name');
        }

        $this->commentsID = $creds['id'];
        $this->name = $creds['name'];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTVCookId(): int
    {
        return $this->tvcookId;
    }

    public function setTVCookId(int $id): void
    {
        $this->tvcookId = $id;
    }

    public function getId(): int
    {
        return (int)$this->commentsID;
    }
}
