<?php

declare(strict_types=1);

namespace App\DTO;

use Exception;

class Comment
{
    private $text;
    private $id;
    private $tvCookRecipeId;

    /**
     * @throws Exception
     */
    public function __construct(array $data)
    {
        if (!array_key_exists('id', $data) || !array_key_exists('comment', $data) || !array_key_exists('tvcook_id', $data)) {
            throw new Exception('Fetched comment has needed components');
        }

        $this->text = $data['comment'];
        $this->id = $data['id'];
        $this->tvCookRecipeId = $data['tvcook_id'];
    }

    public function getId(): int
    {
        return (int)$this->id;
    }

    public function getText(): string
    {
        return (string)$this->text;
    }

    public function getTVCookRecipeId(): int
    {
        return (int)$this->tvCookRecipeId;
    }
}
