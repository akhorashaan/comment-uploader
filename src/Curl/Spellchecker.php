<?php

namespace App\Curl;

use Exception;

class Spellchecker
{
    private const REQUEST_URL = 'https://speller.yandex.net/services/spellservice.json/checkText?text=';

    /**
     * @throws Exception
     */
    public function check(string $text): array
    {
        $ch = curl_init(self::REQUEST_URL . urlencode($text));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);

        if ($response === false) {
            throw new Exception(curl_error($ch), curl_errno($ch));
        }

        curl_close($ch);

        return json_decode($response);
    }
}