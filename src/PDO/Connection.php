<?php

declare(strict_types=1);

namespace App\PDO;

use PDO;

class Connection
{
    private $connection;

    public function __construct(
        string $servername,
        string $database,
        string $username,
        string $password
    ) {
        $sql = "mysql:host=$servername;dbname=$database";
        $dsnOptions = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        ];

        $this->connection = new PDO($sql, $username, $password, $dsnOptions);
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
