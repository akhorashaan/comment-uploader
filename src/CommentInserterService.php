<?php

declare(strict_types=1);

namespace App;

use App\DTO\Comment;
use App\DTO\User;
use App\PDO\Connection;
use App\Repository\Comments;
use App\Repository\TVCook;
use PDO;
use Throwable;

class CommentInserterService
{
    private $commentsRepo;
    private $tvcookRepo;
    private $logger;

    public function __construct(FileLogger $logger, TVCook $tvcookRepo, Comments $commentRepo)
    {
        $this->logger = new $logger;
        $this->tvcookRepo = $tvcookRepo;
        $this->commentsRepo = $commentRepo;
    }

    public function uploadComments($maximumUploads): void
    {
        try {
            for ($i = 1; $i <= $maximumUploads; $i++) {
                $user = $this->commentsRepo->getUser();
                $tvcookUserId = $this->tvcookRepo->createUser($user);
                $user->setTVCookId($tvcookUserId);
                $comment = new Comment($this->commentsRepo->getComment());
                $this->tvcookRepo->createComment($comment, $user);
                $this->commentsRepo->makeUserUsed($user);
                $this->commentsRepo->makeCommentUsed($comment);
                $this->logger->log('info', 'All ok, proceed to next comment');
            }

            $this->logger->log('info', 'New portion of comments upload successfully');
        } catch (Throwable $e) {
            $this->logger->log('error', $e->getMessage() . '  ' . $e->getTraceAsString());
        }
    }
}
