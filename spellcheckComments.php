<?php


use App\CommentsSpellFixer;
use App\Curl\Spellchecker;
use App\FileLogger;
use App\PDO\Connection;
use App\Repository\Comments;

require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$config = include 'config.php';
$logger = new FileLogger();

function connectCommentDb(array $config): PDO
{
    $connection = new Connection($config['comment']['host'], $config['comment']['dbname'], $config['comment']['username'], $config['comment']['password']);

    return $connection->getConnection();
}

$commentConnection = connectCommentDb($config);
$spellFixer = new CommentsSpellFixer(new Comments($commentConnection), new Spellchecker());
$spellFixer->checkAndFix();
